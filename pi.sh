#!/usr/bin/env bash
GOOS=linux GOARCH=arm go build -o build/uniprogress uniprogress.go
ssh pi@192.168.43.101 sudo systemctl stop uniprogress
scp build/uniprogress pi@192.168.43.101:~/
ssh pi@192.168.43.101 sudo systemctl start uniprogress
