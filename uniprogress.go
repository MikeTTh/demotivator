package main

import (
	"./output"
	"fmt"
	"golang.org/x/image/font"
	"golang.org/x/image/font/basicfont"
	"golang.org/x/image/math/fixed"
	"image"
	"image/color"
	"io"
	"os"
	"sync"
	"time"
)

type udate struct {
	Date  time.Time
	Type  string
	Class string
}

var udates []udate

func drawText(scr *[128][64]bool, text string, x, y float64) {
	img := image.NewGray(image.Rect(0, 0, 128, 64))
	col := color.White
	point := fixed.Point26_6{X: fixed.Int26_6(x * 30 * 64), Y: fixed.Int26_6(y * 30 * 64)}

	d := &font.Drawer{
		Dst:  img,
		Src:  image.NewUniform(col),
		Face: basicfont.Face7x13,
		Dot:  point,
	}
	d.DrawString(text)

	for x := 0; x < 128; x++ {
		for y := 0; y < 64; y++ {
			if img.GrayAt(x, y).Y > 50 {
				scr[x][y] = true
			}
		}
	}
}

func drawLooper() {
	var wg sync.WaitGroup
	wg.Add(1)

	screen := [128][64]bool{}
	sept := time.Date(2019, time.September, 9, 0, 0, 0, 0, time.Local)
	end := time.Date(2020, time.January, 30, 0, 0, 0, 0, time.Local)

	go func() {
		wg.Wait()
		for true {
			go func() {
				screen = [128][64]bool{}

				t := time.Now()
				all := end.Sub(sept)
				now := end.Sub(t)
				p := 1.0 - now.Seconds()/all.Seconds()

				drawText(&screen, "<-ZH", 3.2, 0.35)
				drawText(&screen, fmt.Sprintf("%f%%", p*100), 1, 1)
				drawText(&screen, fmt.Sprintf("%4d.%02d.%02d.", t.Year(), t.Month(), t.Day()), 1, 1.5)
				drawText(&screen, fmt.Sprintf("%02d:%02d:%02d", t.Hour(), t.Minute(), t.Second()), 1, 2)

				for _, d := range udates {
					dp := d.Date.Sub(sept).Seconds() / all.Seconds()
					index := int(128.0 * dp)
					dotornot := d.Type == "hf"
					for j := 0; j < 15; j++ {
						if dotornot {
							screen[index][j] = screen[index][j] || (j%2 == 0)
						} else {
							screen[index][j] = true
						}
					}
				}

				ppix := 128.0 * p

				in := int(ppix)
				jn := int((ppix - float64(in)) * 64)

				for i := 0; i < in; i++ {
					for j := 0; j < 64; j++ {
						screen[i][j] = !screen[i][j] // != ((float64(i)/128.0) < p)
					}
				}
				for j := 0; j < jn; j++ {
					screen[in][j] = !screen[in][j]
				}

				output.Draw(&screen)
				output.Backlight(!(t.Hour() >= 23 || t.Hour() < 7))
			}()
			time.Sleep(time.Second)
		}
	}()

	output.Init(&wg)
}

func main() {
	f, err := os.Open("./data.txt")
	if err != nil {
		panic(err)
	}
	for true {
		d := udate{}
		m, da := 0, 0
		i, err := fmt.Fscanf(f, "%d %d %s %s", &m, &da, &(d.Type), &(d.Class))
		if i <= 0 || err == io.EOF {
			break
		}
		if err != nil || (i < 4 && i > 0) {
			continue
		}

		dt := time.Date(2019, time.Month(m), da, 0, 0, 0, 0, time.Local)

		d.Date = dt

		udates = append(udates, d)
	}

	drawLooper()
}
